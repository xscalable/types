package types

import (
	"database/sql/driver"
	"time"
)

type NullTime struct {
	Time  time.Time `json:"time"`
	Valid bool      `json:"-"` // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}

// Marshal encodes Raw to slice of bytes. Exists to fit gogoprotobuf custom
// type interface.
func (r NullTime) Marshal() ([]byte, error) {
	if !r.Valid {
		return nil, nil
	}
	return r.Time.MarshalJSON()
}

// MarshalTo exists to fit gogoprotobuf custom type interface.
func (nt NullTime) MarshalTo(data []byte) (n int, err error) {
	if !nt.Valid {
		return 0, nil
	}
	bytes, err := nt.Time.MarshalBinary()
	if err != nil {
		return 0, nil
	}
	copy(data, bytes)
	return len(bytes), nil
}

// Unmarshal exists to fit gogoprotobuf custom type interface.
func (nt *NullTime) Unmarshal(data []byte) error {
	if len(data) == 0 {
		nt.Valid = false
		return nil
	}
	if err := nt.Time.UnmarshalBinary(data); err != nil {
		nt.Valid = false
		return err
	}
	nt.Valid = true
	return nil
}

// Size exists to fit gogoprotobuf custom type interface.
func (nt *NullTime) Size() int {
	if nt.Valid == false {
		return 0
	}
	bytes, err := nt.Time.MarshalBinary()
	if err != nil {
		return 0
	}
	return len(bytes)
}
